export interface Product {
    id: number,
    name: string,
    price: number,
}

export interface IProductRepository {
    getAllProducts(): Promise<Product[]>;
}