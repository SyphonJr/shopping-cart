import { TIMEOUT } from "node:dns";
import { IProductRepository, Product } from "./i_product_repository";

// export default class ProductRepository implements IProductRepository {
//     async getAllProducts(): Promise<Product[]> {

//         setTimeout(() => {
//         }, 100);
//         return [{ id: 1232, name: "cargo pants", price: 70 }, { id: 1234, name: "T-Shirt", price: 45 }];
//     }
// }

export const getAllProducts = async (): Promise<Product[]> => {
    await new Promise(resolve => setTimeout(resolve, 3000));
    return [{ id: 1232, name: "cargo pants", price: 70 }, { id: 1234, name: "T-Shirt", price: 45 }];
}