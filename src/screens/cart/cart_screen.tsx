import { cartListState, cartDetailsState, ProductWithQuantity } from '../../recoil/cart';
import { useRecoilState, useRecoilValue } from 'recoil';
import { useEffect, useState } from 'react';
import { shippingListState } from '../../recoil/shipping';
import { parseIsolatedEntityName } from 'typescript';

const CartScreen = () => {

    return (<div className="flex space-x-4">
        <ProductsInCartView />
        <SummaryView />
    </div>);
}


export default CartScreen;

const ProductsInCartView = () => {

    const [cartList, setCartList] = useRecoilState(cartListState);

    const handleQuantityChange = (index: number) => (event: React.FormEvent<HTMLInputElement>) => {
        console.log(event.currentTarget.value);
        let newCartList = [...cartList];

        // TODO: Check if event.value is parseable first.

        newCartList[index] = { ...cartList[index], quantity: parseInt(event.currentTarget.value) < 0 ? 0 : parseInt(event.currentTarget.value) };
        setCartList(newCartList);
    }

    return (<div className="mr-auto bg-gray-50 w-full">
        <p className="text-3xl font-bold">Shopping Cart</p>

        <table className="table-auto">
            <thead>
                <tr className="border-t-2 border-b-2">
                    <th>PRODUCT DETAILS</th>
                    <th>QUANTITY</th>
                    <th>PRICE</th>
                    <th>TOTAL</th>
                </tr>
            </thead>
            <tbody>
                {
                    cartList.map((product, index) => {
                        return (
                            <tr>
                                <td>{product.product.name}</td>
                                <td><input type="number" className="w-10" name="quantity" value={product.quantity} onChange={handleQuantityChange(index)}></input>
                                </td>
                                <td>{product.product.price}</td>
                                <td>{product.quantity * product.product.price}</td>
                            </tr>)
                    })
                }
            </tbody>
        </table>
    </div>)
}

const SummaryView = () => {
    const cartDetails = useRecoilValue(cartDetailsState);
    const shippingList = useRecoilValue(shippingListState);

    const [selectIndex, setSelectIndex] = useState(0);

    const handleSelectChange = (event: React.FormEvent<HTMLSelectElement>) => {
        // TODO: Check if event value is parseable first.
        setSelectIndex(parseInt(event.currentTarget.value));
    }

    useEffect(() => {
        console.log(selectIndex);
    });

    return (
        <div className="w-72 bg-gray-100 rounded-md p-8 flex flex-col">
            <div className="pb-4 border-b border-gray-300">
                <h2 className="font-bold">ORDER SUMMARY</h2>
            </div>
            <div className="flex mt-4">
                <p className="font-bold">ITEMS {cartDetails.totalProducts}</p>
                <p className="font-bold ml-auto">${cartDetails.totalPrice}</p>
            </div>
            <div className="pt-4 pb-8 border-b border-gray-300">
                <p className="font-bold mb-2">SHIPPING</p>
                <select className="w-full content-center" value={selectIndex} onChange={handleSelectChange}>
                    {
                        shippingList.map((shipping, index) => {

                            return <option key={shipping.name} value={index}>{shipping.name} - ${shipping.price}</option>
                        })
                    }
                </select>
            </div>
            <div className="flex my-4">
                <p className="font-bold">TOTAL COST </p>
                <p className="font-bold ml-auto">${cartDetails.totalPrice + shippingList[selectIndex].price}</p>

            </div>
            <button className="w-full bg-gray-700 hover:bg-gray-600 active:bg-gray-900 text-white rounded-sm h-10 align-middle font-semibold">CHECKOUT</button>

        </div>
    )
}