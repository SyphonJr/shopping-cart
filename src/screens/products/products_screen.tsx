import { useRecoilState, useRecoilStateLoadable, useSetRecoilState } from "recoil";
import { cartListState, ProductWithQuantity } from "../../recoil/cart";
import { productList } from "../../recoil/products";
import { Product } from "../../services/database/i_product_repository";

const ProductsScreen = () => {
    const [productListState, setProductList] = useRecoilStateLoadable(productList);
    console.log(productListState.state);
    if (productListState.state === "loading") {
        return <div><p>Loading</p></div>
    }
    if (productListState.state === "hasValue") {
        return (<div>
            <ProductsListView products={productListState.contents}></ProductsListView>
        </div>)
    }
    if (productListState.state === "hasError") {
        return <div><p>error</p></div>
    }

    return <div>No results</div>
}

export default ProductsScreen;


interface ProductsList {
    products: Product[],
}

const ProductsListView = (props: ProductsList) => {

    const [cartList, setCartList] = useRecoilState(cartListState);

    const productClickedHandler = (product: Product) => ({ }) => {
        const index = cartList.findIndex(p => p.product.id == product.id);
        if (index == -1) {
            setCartList([...cartList, { product, quantity: 1 }]);
        }
        else {
            let newCartList = [...cartList] as ProductWithQuantity[];
            let productToUpdate = { ...newCartList[index], quantity: newCartList[index].quantity + 1 };
            newCartList[index] = productToUpdate;

            setCartList(newCartList);
        }
    }
    return <div>
        {
            props.products.map((product: Product) => {
                return <div>
                    <p>{product.name}</p>
                    <button onClick={productClickedHandler(product)}>Add to cart</button>
                </div>
            })
        }
    </div>
}