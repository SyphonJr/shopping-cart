import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useHistory
} from "react-router-dom";
import Home from './home/home_screen';
import Cart from './cart/cart_screen';
import Products from './products/products_screen';
import { cartDetailsState } from '../recoil/cart';
import { useRecoilValue } from 'recoil';

const App = () => {

  const history = useHistory();

  const cartDetails = useRecoilValue(cartDetailsState);
  let cartItems: string = cartDetails.totalProducts > 0 ? `(${cartDetails.totalProducts})` : "";

  const handleCartClick = () => {
    history.push("/cart")
  }

  return (
    <div>
      <div className="py-4 bg-gray-700 flex px-16 text-white">
        <a href="#" className="">UNITED KINGDOM</a>
        <div className="flex ml-auto gap-12">
          <a href="#">SIGN IN</a>
          <a href="#">WISHLIST</a>
        </div>
      </div>

      <nav className="flex mx-16 py-10 align-center">
        <div className="">
          <h1 className="text-4xl">NEXLINE</h1>
        </div>
        <ul className="flex items-center mx-auto gap-12">
          <li className="">
            <Link to="/">Home</Link>
          </li>
          <li className="">
            <Link to="/products">Products  </Link>
          </li>
        </ul>
        <div className="flex items-center justify-self-end gap-8">
          <div className="rounded flex border border-gray-400 w-52 h-8 items-center px-2">
            <img src="https://img.icons8.com/ios-filled/50/000000/search--v1.png" className="object-contain h-6" />
            <input type="text" placeholder="Search" className="min-w-0 ml-2 px-1 appearance-none focus:outline-none"></input>
          </div>
          <button onClick={handleCartClick} className="rounded flex border border-gray-400 px-2 gap-2 h-8 items-center"><img src="https://img.icons8.com/material-sharp/24/000000/shopping-cart.png" className="object-contain h-6" /><span>Cart</span></button>
        </div>
      </nav>

      <div className="flex h-12">
        <div className="bg-gray-100 flex-1 pl-12 flex justify-center items-center"><p>FREE NEXT DAY DELIVERY</p></div>
        <div className="bg-blue-300 flex-1 flex justify-center items-center text-white"><p>SALE NOW UP TO 50%</p></div>
        <div className="bg-gray-100 flex-1 pr-12 flex justify-center items-center"><p>FREE RETURNS TO STORE</p></div>
      </div>

      {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
      <div className="mt-16 mx-16">
        <Switch>
          <Route path="/products">
            <Products />
          </Route>
          <Route path="/cart">
            <Cart />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </div>
  );
}

export default App;