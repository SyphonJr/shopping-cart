import { atom, selector } from "recoil";
import { IProductRepository, Product } from "../services/database/i_product_repository";
import { getAllProducts } from "../services/database/product_repository";

export const allProductsState = selector<Product[]>({
    key: "allUsersState",
    get: async ({ get }) => {
        let products: Product[] = [];
        try {
            products = await getAllProducts();
            return products;
        }
        catch (err) {
            console.log("Error happened while fetching products")
            return products;
        }

    }
});

export const productList = atom({ key: "usersListState", default: allProductsState });


