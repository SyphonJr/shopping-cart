import { atom, selector } from 'recoil';

export interface Shipping {
    name: string,
    price: number,
}

export const shippingListState = atom({ key: 'shippingListState', default: [{ name: "Standard Delivery", price: 4, }, { name: "Express delivery", price: 8 }], },);
