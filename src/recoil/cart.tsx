import { atom, selector } from 'recoil';
import { Product } from '../services/database/i_product_repository';

export interface ProductWithQuantity {
    product: Product,
    quantity: number,
}

export const cartListState = atom({ key: 'cartListState', default: [{ product: { id: 1232, name: "cargo pants", price: 70 }, quantity: 3 }, { product: { id: 1234, name: "T-Shirt", price: 45 }, quantity: 10 }] as ProductWithQuantity[] });

// Selector makes it possible to create derived state.
export const cartDetailsState = selector({
    key: 'cartDetailsState', get: ({ get }) => {
        const cartList: ProductWithQuantity[] = get(cartListState);

        let products = 0;
        let price = 0;
        cartList.forEach(element => {
            price += element.product.price * element.quantity;
            products += element.quantity;
        });
        const totalProducts = products;
        const totalPrice = price;

        return { totalProducts, totalPrice }
    }
});